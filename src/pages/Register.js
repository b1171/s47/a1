import {useState, useEffect} from "react";
import {Container, Row, Col, Form, Button} from "react-bootstrap";

export default function  Register() {

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [cp, setCp] = useState("");
	const [isActive, setIsActive] = useState(false);

	function registerUser(e){
		e.preventDefault();

		setEmail("");
		setPassword("");
		setCp("")

		alert("Thank you for registering to React Booking!");
	}

	useEffect(() => {

		if( email !== "" && 
			password !== "" && 
			cp !== "" &&
			password === cp ){

			setIsActive(true);
		}else{
			setIsActive(false);
		}

	}, [email, password, cp]);

	return (
		<Container className="my-5">
			<Row className="justify-content-center">
				<Col xs={10} md={6}>
					<Form className="border p-3" onSubmit={e => registerUser(e)}>

						<Form.Group className="mb-3" controlId="formBasicEmail">
							<Form.Label>Email Address</Form.Label>
							<Form.Control type="email" placeholder="Enter email" value={email}
								onChange={e => {setEmail(e.target.value)}}></Form.Control>
						</Form.Group>

						<Form.Group className="mb-3" controlId="formBasicPassword">
							<Form.Label>Password</Form.Label>
							<Form.Control type="password" placeholder="Enter password" value={password}
								onChange={e => {setPassword(e.target.value)}}></Form.Control>
						</Form.Group>

						<Form.Group className="mb-3" controlId="formBasicConfirmPassword">
							<Form.Label>Confirm Password</Form.Label>
							<Form.Control type="password" placeholder="Confirm password" value={cp}
								onChange={e => {setCp(e.target.value)}}></Form.Control>
						</Form.Group>
						
						<Form.Group>
							<Button variant="primary" type="submit" disabled={!isActive}>Submit</Button>
						</Form.Group>

{/*						<Form.Group>
						{
					    	isActive ?
					    		<Button
					    			variant="primary"
					    			type="submit"
					    		>
					    			Submit
					    		</Button>
					    	:
					    		<Button
					    			variant="primary"
					    			type="submit"
					    			disabled
					    		>
					    			Submit
					    		</Button>

					    }
						</Form.Group>*/}

					</Form>
				</Col>
			</Row>	
		</Container>
	)
}